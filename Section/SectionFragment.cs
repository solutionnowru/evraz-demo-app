﻿using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.ConstraintLayout.Widget;
using AndroidX.Fragment.App;
using AndroidX.ViewPager.Widget;
using Google.Android.Material.BottomSheet;
using Google.Android.Material.Tabs;
using EvrazDemo.Helpers;
using System;
using System.Collections.Generic;

namespace EvrazDemo.Section
{
    public class SectionFragment : Fragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.section_fragment, container, false);

            var backButton = view.FindViewById(Resource.Id.backButton);
            var bottomSheet = view.FindViewById(Resource.Id.bottomSheet);
            var bottomSheetBehavior = BottomSheetBehavior.From(bottomSheet);
            var goToWorkshop = view.FindViewById(Resource.Id.workshop);
            var headCall = view.FindViewById(Resource.Id.sectionHeadCallButton);

            bottomSheetBehavior.State = BottomSheetBehavior.StateHidden;

            LayoutSegments(inflater, view, bottomSheetBehavior);

            SetupViewPager(inflater, view, bottomSheetBehavior);

            backButton.Click += (s, e) => Activity.OnBackPressed();

            goToWorkshop.Click += (s, e) =>
            {
                ParentFragmentManager.BeginTransaction()
                    .Add(Id, Java.Lang.Class.FromType(typeof(ToBeImplementedFragment)), null)
                    .AddToBackStack(null)
                    .Commit();
            };

            headCall.Click += (s, e) =>
            {
                var dialIntent = new Intent(Intent.ActionDial);
                dialIntent.SetData(Android.Net.Uri.Parse("tel:+79278567421"));
                Context.StartActivity(dialIntent);
            };

            return view;
        }

        private void SetupViewPager(LayoutInflater inflater, View view, BottomSheetBehavior bottomSheetBehavior)
        {
            var pager = view.FindViewById<ViewPager>(Resource.Id.pager);
            var pagerTabs = view.FindViewById<TabLayout>(Resource.Id.pagerTab);

            pager.Adapter = new SectionDetailsPagerAdapter(ChildFragmentManager, FragmentPagerAdapter.BehaviorResumeOnlyCurrentFragment);
            pagerTabs.SetupWithViewPager(pager);

            var incidentsTab = pagerTabs.GetTabAt(0);
            var incidentsTabView = inflater.Inflate(Resource.Layout.section_details_tabbar_item, incidentsTab.View, false);
            incidentsTab.SetCustomView(incidentsTabView);
            var incidentsTabTitle = incidentsTabView.FindViewById<TextView>(Resource.Id.title);
            incidentsTabTitle.Text = "Происшествия";
            incidentsTab.View.Post(() =>
            {
                var layoutParams = incidentsTab.View.LayoutParameters as LinearLayout.LayoutParams;
                layoutParams.Weight += 0.2f;
                incidentsTab.View.RequestLayout();
            });

            var performanceTab = pagerTabs.GetTabAt(1);
            var performanceTabView = inflater.Inflate(Resource.Layout.section_details_tabbar_item, performanceTab.View, false);
            performanceTab.SetCustomView(performanceTabView);
            var performanceTabTitle = performanceTabView.FindViewById<TextView>(Resource.Id.title);
            performanceTabTitle.Text = "Показатели";

            var statisticsTab = pagerTabs.GetTabAt(2);
            var statisticsTabView = inflater.Inflate(Resource.Layout.section_details_tabbar_item, statisticsTab.View, false);
            statisticsTab.SetCustomView(statisticsTabView);
            var statisticsTabTitle = statisticsTabView.FindViewById<TextView>(Resource.Id.title);
            statisticsTabTitle.Text = "Статистика";

            incidentsTab.View.Click += OnTabClicked;
            performanceTab.View.Click += OnTabClicked;
            statisticsTab.View.Click += OnTabClicked;

            void OnTabClicked(object sender, System.EventArgs e)
            {
                bottomSheetBehavior.State = BottomSheetBehavior.StateExpanded;
            }
        }

        private void LayoutSegments(LayoutInflater inflater, View view, BottomSheetBehavior bottomSheetBehavior)
        {
            var segmentTitle = view.FindViewById<TextView>(Resource.Id.segmentTitle);

            var hexCenter = view.FindViewById(Resource.Id.hexCenter);
            var content = view.FindViewById<ConstraintLayout>(Resource.Id.content);

            var radius = (int)(Resources.DisplayMetrics.WidthPixels * 0.326);

            var segmentsCount = new Random().Next(6, 8);

            var hexes = new List<ImageView>();

            for (int i = 0; i < segmentsCount; i++)
            {
                var segment = inflater.Inflate(Resource.Layout.hexagon_notification_text, content, false);
                segment.Id = View.GenerateViewId();

                var hex = segment.FindViewById<ImageView>(Resource.Id.hex);
                var badge = segment.FindViewById<View>(Resource.Id.badge);
                var badgeCount = segment.FindViewById<TextView>(Resource.Id.badgeText);

                hexes.Add(hex);

                if (i == 0 || i == 3)
                {
                    badge.Visibility = ViewStates.Visible;
                    badgeCount.Text = "3";
                }

                content.AddView(segment, new ConstraintLayout.LayoutParams(segment.LayoutParameters)
                {
                    CircleConstraint = hexCenter.Id,
                    CircleRadius = radius,
                    CircleAngle = i * 360 / 8
                });

                var label = new TextView(Context)
                {
                    TextSize = 9,
                    Text = $"Сегмент {i+1}"
                };
                label.SetAllCaps(true);
                label.SetTextColor(Color.Rgb(65, 79, 79));

                content.AddView(label, new ConstraintLayout.LayoutParams(
                    ViewGroup.LayoutParams.WrapContent,
                    ViewGroup.LayoutParams.WrapContent)
                {
                    TopToBottom = segment.Id,
                    TopMargin = 8,
                    StartToStart = segment.Id,
                    EndToEnd = segment.Id
                });

                segment.Click += (s, e) =>
                {
                    hexes.ForEach(h => h.SetImageResource(Resource.Drawable.hexagon));
                    hex.SetImageResource(Resource.Drawable.hexagon_highlighted);

                    bottomSheetBehavior.State = BottomSheetBehavior.StateCollapsed;
                    segmentTitle.Text = label.Text;
                };
            }
        }
    }
}