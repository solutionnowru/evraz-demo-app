﻿using Android.Runtime;
using AndroidX.Fragment.App;
using EvrazDemo.IncidentList;
using System;

namespace EvrazDemo.Section
{
    internal class SectionDetailsPagerAdapter : FragmentPagerAdapter
    {
        protected SectionDetailsPagerAdapter(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public SectionDetailsPagerAdapter(AndroidX.Fragment.App.FragmentManager fm, int behavior) : base(fm, behavior)
        {
        }

        public override int Count => 3;

        public override AndroidX.Fragment.App.Fragment GetItem(int position)
        {
            switch (position)
            {
                case 0:
                    return new IncidentListFragment();

                case 1:
                case 2:
                    return new ToBeImplementedFragment();

                default:
                    throw new ArgumentOutOfRangeException(nameof(position));
            }
        }
    }
}