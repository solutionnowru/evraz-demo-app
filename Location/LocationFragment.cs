﻿using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;

namespace EvrazDemo.Location
{
    internal class LocationFragment : Fragment, IOnMapReadyCallback
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.location_fragment, container, false);

            var mapFragment = (SupportMapFragment)ChildFragmentManager.FindFragmentById(Resource.Id.map);
            mapFragment.GetMapAsync(this);

            var backButton = view.FindViewById(Resource.Id.backButton);
            backButton.Click += (s, e) => Activity.OnBackPressed();

            return view;
        }

        public void OnMapReady(GoogleMap map)
        {
            map.UiSettings.CompassEnabled = true;

            var location = new LatLng(57.932178, 60.029557);

            map.AddMarker(new MarkerOptions()
                .SetPosition(location)
                .SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.ic_location_mark)));

            map.MoveCamera(CameraUpdateFactory.NewCameraPosition(CameraPosition.InvokeBuilder()
                .Target(location)
                .Zoom(17)
                .Build()));
        }
    }
}