﻿using Android.OS;
using Android.Views;
using AndroidX.Fragment.App;

namespace EvrazDemo
{
    public class ToBeImplementedFragment : Fragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            return inflater.Inflate(Resource.Layout.to_be_implemented_fragment, container, false);
        }
    }
}