﻿using Android.App;

namespace EvrazDemo.Helpers
{
    public static class Converter
    {
        public static int SpToPx(float spValue)
        {
            var fontScale = Application.Context.Resources.DisplayMetrics.ScaledDensity;
            return (int)((spValue * fontScale) + 0.5f);
        }

        public static int DpToPx(float dpValue)
        {
            var fontScale = Application.Context.Resources.DisplayMetrics.Density;
            return (int)((dpValue * fontScale) + 0.5f);
        }
    }
}