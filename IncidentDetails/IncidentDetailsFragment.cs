﻿using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using AndroidX.RecyclerView.Widget;
using ObservableRecycler;
using EvrazDemo.Location;
using Square.Picasso;
using System;
using System.Linq;
using System.Reactive.Disposables;

namespace EvrazDemo.IncidentDetails
{
    internal class IncidentDetailsFragment : Fragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.incident_details_fragment, container, false);

            var backButton = view.FindViewById(Resource.Id.backButton);
            backButton.Click += (s, e) => Activity.OnBackPressed();

            var photos = view.FindViewById<RecyclerView>(Resource.Id.photos);
            photos.SetAdapter(new ObservableRecyclerAdapter<int, CachingViewHolder>
            {
                DataSource = Enumerable.Range(0, 10).ToList(),
                CellLayoutId = Resource.Layout.incident_photo_item,
                BindViewHolderDelegate = OnBindPhotoItem,
            });

            var responsibleLocate = view.FindViewById(Resource.Id.responsibleLocateButton);
            responsibleLocate.Click += (s, e) =>
            {
                ParentFragmentManager.BeginTransaction()
                    .Add(Id, Java.Lang.Class.FromType(typeof(LocationFragment)), null)
                    .AddToBackStack(null)
                    .Commit();
            };

            var responsibleCall = view.FindViewById(Resource.Id.responsibleCallButton);
            var headCall = view.FindViewById(Resource.Id.headCallButton);

            responsibleCall.Click += OnCallClick;
            headCall.Click += OnCallClick;

            void OnCallClick(object sender, EventArgs e)
            {
                var dialIntent = new Intent(Intent.ActionDial);
                dialIntent.SetData(Android.Net.Uri.Parse("tel:+79992196960"));
                Context.StartActivity(dialIntent);
            }

            return view;
        }

        private void OnBindPhotoItem(CachingViewHolder holder, int model, int position, CompositeDisposable disposable)
        {
            var image = holder.FindCachedView<ImageView>(Resource.Id.image);

            if (image.Drawable == null)
            {
                Picasso.With(Context)
                    .Load("https://source.unsplash.com/100x100/?metal,factory")
                    .Placeholder(new ColorDrawable(Color.Rgb(229, 229, 229)))
                    .MemoryPolicy(MemoryPolicy.NoStore)
                    .Into(image);
            }
        }
    }
}