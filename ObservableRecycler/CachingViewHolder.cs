﻿using Android.Runtime;
using Android.Views;
using AndroidX.RecyclerView.Widget;
using System;
using System.Collections.Generic;

namespace ObservableRecycler
{
    /// <summary>
    /// An extension of <see cref="RecyclerView.ViewHolder"/> optimized for usage with the
    /// <see cref="ObservableRecyclerAdapter{TItem, THolder}"/>.
    /// </summary>
    public class CachingViewHolder : RecyclerView.ViewHolder
    {
        private readonly Dictionary<int, View> _cachedSubViews = new Dictionary<int, View>();

        public CachingViewHolder(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }

        public CachingViewHolder(View itemView)
            : base(itemView)
        {
        }

        /// <summary>
        /// Explores the attached view and returns the UI element corresponding
        /// to the viewId. If no element is found with this ID, the method returns null.
        /// </summary>
        /// <typeparam name="TView">The type of the view to be returned.</typeparam>
        /// <param name="viewId">The ID of the subview that needs to be retrieved.</param>
        /// <returns>The sub view corresponding to the viewId, or null if no corresponding sub view is found.</returns>
        public TView FindCachedView<TView>(int viewId)
            where TView : View
        {
            if (_cachedSubViews.ContainsKey(viewId))
                return _cachedSubViews[viewId] as TView;

            var view = ItemView.FindViewById<TView>(viewId);

            if (view != null)
                _cachedSubViews.Add(viewId, view);

            return view;
        }

        /// <summary>
        /// Explores the attached view and returns the UI element corresponding
        /// to the viewId. If no element is found with this ID, the method returns null.
        /// </summary>
        /// <param name="viewId">The ID of the subview that needs to be retrieved.</param>
        /// <returns>The sub view corresponding to the viewId, or null if no corresponding sub view is found.</returns>
        public View FindCachedView(int viewId)
        {
            return FindCachedView<View>(viewId);
        }
    }
}