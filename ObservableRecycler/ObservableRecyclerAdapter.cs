﻿using Android.Views;
using AndroidX.RecyclerView.Widget;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reactive.Disposables;

namespace ObservableRecycler
{
    /// <summary>
    /// A <see cref="RecyclerView.Adapter"/> that automatically updates the associated <see cref="RecyclerView"/> when its
    /// data source changes. Note that the changes are only observed if the data source
    /// implements <see cref="INotifyCollectionChanged"/>.
    /// </summary>
    /// <typeparam name="TItem">The type of the items in the data source.</typeparam>
    /// <typeparam name="THolder">The type of the <see cref="RecyclerView.ViewHolder"/> used in the RecyclerView.
    /// For better results and simpler implementation, you can use a <see cref="CachingViewHolder"/> or
    /// provide your own implementation.</typeparam>
    ////[ClassInfo(typeof(ObservableAdapter<T>)]
    public class ObservableRecyclerAdapter<TItem, THolder> : RecyclerView.Adapter
        where THolder : RecyclerView.ViewHolder
    {
        private IList<TItem> _dataSource;
        private INotifyCollectionChanged _notifier;
        private readonly IDictionary<RecyclerView.ViewHolder, CompositeDisposable> _holderDisposables = new Dictionary<RecyclerView.ViewHolder, CompositeDisposable>();

        /// <summary>
        /// A delegate to a method taking a <see cref="RecyclerView.ViewHolder"/>
        /// and setting its View's properties according to the item
        /// passed as second parameter. CompositeDisposable will be disposed
        /// when holder's view is recycled.
        /// </summary>
        public Action<THolder, TItem, int, CompositeDisposable> BindViewHolderDelegate { get; set; }

        /// <summary>
        /// The Resource ID of the AXML file we should use to create
        /// cells for the RecyclerView. Alternatively you can use the
        /// <see cref="CreateViewHolderDelegate"/> property.
        /// </summary>
        public int CellLayoutId { get; set; }

        /// <summary>
        /// A delegate to a callback that will be called when an item
        /// in the list is clicked (or tapped) by the user. This can be used
        /// to perform UI operations such as changing the background color, etc.
        /// </summary>
        public Action<THolder, TItem, int> ClickCallback { get; set; }

        /// <summary>
        /// A delegate to a method taking an item's position and
        /// a <see cref="RecyclerView.ViewHolder"/> and creating and returning
        /// a cell for the RecyclerView. Alternatively you can use the
        /// <see cref="CellLayoutId"/> property.
        /// </summary>
        public Func<ViewGroup, int, THolder> CreateViewHolderDelegate { get; set; }

        /// <summary>
        /// A delegate to a method taking an item's position and returning the view type.
        /// If null, defult view type will always be 0.
        /// </summary>
        public Func<int, int> GetItemViewTypeDelegate { get; set; }

        /// <summary>
        /// A delegate to a method taking an item's position and returning the item id.
        /// Used if <see cref="HasStableIds"/> set to true.
        /// If null, defult id will always be 0.
        /// </summary>
        public Func<int, TItem, long> GetItemIdDelegate { get; set; }

        /// <summary>
        /// The data source of this list adapter.
        /// </summary>
        public IList<TItem> DataSource
        {
            get => _dataSource;
            set
            {
                if (Equals(_dataSource, value))
                    return;

                if (_notifier != null)
                    _notifier.CollectionChanged -= HandleCollectionChanged;

                _dataSource = value;
                _notifier = value as INotifyCollectionChanged;

                if (_notifier != null)
                    _notifier.CollectionChanged += HandleCollectionChanged;

                NotifyDataSetChanged();
            }
        }

        /// <summary>
        /// Gets the number of items in the data source.
        /// </summary>
        public override int ItemCount =>
            _dataSource == null ? 0 : _dataSource.Count;

        /// <summary>
        /// Gets an item corresponding to a given position.
        /// </summary>
        /// <param name="position">The position of the item.</param>
        /// <returns>An item corresponding to a given position.</returns>
        public TItem this[int position] =>
            _dataSource == null ? default : _dataSource[position];

        public override long GetItemId(int position)
        {
            return GetItemIdDelegate?.Invoke(position, this[position]) ?? 0;
        }

        /// <summary>
        /// Gets an item corresponding to a given position.
        /// </summary>
        /// <param name="position">The position of the item.</param>
        /// <returns>An item corresponding to a given position.</returns>
        public TItem GetItem(int position) => this[position];

        public override void OnViewRecycled(Java.Lang.Object holder)
        {
            base.OnViewRecycled(holder);

            if (!(holder is RecyclerView.ViewHolder viewHolder))
                return;

            if (_holderDisposables.TryGetValue(viewHolder, out var holderDisposable))
            {
                holderDisposable.Dispose();
                _holderDisposables.Remove(viewHolder);
            }
        }

        /// <summary>
        /// Called when the View should be bound to the represented Item.
        /// </summary>
        /// <param name="holder">The <see cref="RecyclerView.ViewHolder"/> for this item.</param>
        /// <param name="position">The position of the item in the data source.</param>
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            if (BindViewHolderDelegate == null)
                throw new InvalidOperationException(
                    "OnBindViewHolder was called but no BindViewHolderDelegate was found");

            _holderDisposables.TryGetValue(holder, out var holderDisposable);
            holderDisposable?.Dispose();

            holderDisposable = new CompositeDisposable();
            _holderDisposables[holder] = holderDisposable;

            BindViewHolderDelegate((THolder)holder, this[position], position, holderDisposable);
        }

        /// <summary>
        /// Gets the type of the item view and it will be passed to OnCreateViewHolder method
        /// to handle multiple layout based on position.
        /// </summary>
        /// <returns>The item view type.</returns>
        /// <param name="position">Position.</param>
        public override int GetItemViewType(int position)
        {
            if (GetItemViewTypeDelegate == null)
                return 0;

            return GetItemViewTypeDelegate(position);
        }

        /// <summary>
        /// Called when the View should be created.
        /// </summary>
        /// <param name="parent">The parent for the view.</param>
        /// <param name="viewType">The resource ID (unused).</param>
        /// <returns></returns>
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            THolder holder = null;

            if (CellLayoutId != 0)
            {
                var viewHolderType = typeof(THolder);

                var constructor = viewHolderType.GetConstructor(new[] { typeof(View) });

                if (constructor == null)
                    throw new InvalidOperationException(
                        $"No suitable constructor found for {viewHolderType.FullName}");

                var view = LayoutInflater.From(parent.Context).Inflate(CellLayoutId, parent, false);
                holder = constructor.Invoke(new object[] { view }) as THolder;
            }
            else if (CreateViewHolderDelegate == null)
            {
                throw new InvalidOperationException(
                    "OnCreateViewHolder was called but no CreateViewHolderDelegate or CellLayoutId was found");
            }
            else
            {
                holder = CreateViewHolderDelegate(parent, viewType);
            }

            if (holder != null)
                holder.ItemView.Click += (s, e) => OnItemClick(holder, holder.AdapterPosition);

            return holder;
        }

        /// <summary>
        /// Called when an item is clicked (or tapped) in the list.
        /// </summary>
        /// <param name="holder">ViewHolder representing the clicked item.</param>
        /// <param name="position">The position of the clicked item.</param>
        public void OnItemClick(THolder holder, int position)
        {
            if (ClickCallback == null
                || position == RecyclerView.NoPosition
                || position > ItemCount - 1)
            {
                return;
            }

            ClickCallback(holder, GetItem(position), position);
        }

        private void HandleCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        NotifyItemRangeInserted(e.NewStartingIndex, e.NewItems.Count);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        NotifyItemRangeRemoved(e.OldStartingIndex, e.OldItems.Count);
                    }
                    break;

                case NotifyCollectionChangedAction.Replace:
                    {
                        var oldCount = e.OldItems?.Count ?? 0;
                        var newCount = e.NewItems?.Count ?? 0;

                        var changed = Math.Min(oldCount, newCount);

                        if (oldCount > changed)
                        {
                            NotifyItemRangeRemoved(e.OldStartingIndex + changed, oldCount - changed);
                        }
                        else if (newCount > changed)
                        {
                            NotifyItemRangeInserted(e.OldStartingIndex + changed, newCount - changed);
                        }

                        if (changed != 0)
                        {
                            NotifyItemRangeChanged(e.OldStartingIndex, changed);
                        }
                    }
                    break;

                default:
                    NotifyDataSetChanged();
                    break;
            }
        }
    }
}