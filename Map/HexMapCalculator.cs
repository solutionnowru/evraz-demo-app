﻿using System;

namespace EvrazDemo.Map
{
    internal static class HexMapCalculator
    {
        public static float TotalHeight(int hexRadius, int rows)
        {
            return hexRadius * ((rows * 1.5f) + 0.5f);
        }

        public static float TotalWidth(float cellWidth, int oddColumns, int evenColumns)
        {
            return Math.Max(evenColumns, oddColumns) * cellWidth;
        }

        public static float CellHeight(int hexRadius)
        {
            return hexRadius * 2;
        }

        public static float CellWidth(int hexRadius)
        {
            return (float)(hexRadius * Math.Sqrt(3));
        }
    }
}