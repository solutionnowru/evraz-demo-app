﻿using Android.Views;
using System;
using System.Linq;

namespace EvrazDemo.Map
{
    internal class HexLayoutAdapter<T> : IHexLayoutAdapter
    {
        public View[][] Views { get; }
        public int Rows { get; }
        public int EvenCols { get; }
        public int OddCols { get; }

        public HexLayoutAdapter(T[][] data, Func<int, int, T, View> viewFactory)
        {
            Rows = data.Length;
            EvenCols = data[0].Length;
            OddCols = data[1].Length;

            Views = data
                .Select((row, rowIndex) => row
                    .Select((element, colIndex) => viewFactory(rowIndex, colIndex, element))
                    .ToArray())
                .ToArray();
        }
    }
}