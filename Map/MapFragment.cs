﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.ConstraintLayout.Widget;
using AndroidX.Fragment.App;
using Com.Otaliastudios.Zoom;
using EvrazDemo.Section;

namespace EvrazDemo.Map
{
    [Register("Sample.Map.MapFragment")]
    public class MapFragment : Fragment
    {
        private HexGridView _hexBackground;
        private HexLayout _hexGrid;
        private ZoomLayout _zoomLayout;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.map_fragment, container, false);

            _zoomLayout = view.FindViewById<ZoomLayout>(Resource.Id.zoomLayout);
            _zoomLayout.PostDelayed(() => _zoomLayout.ZoomTo(1f, false), 300);

            var zoomPlus = view.FindViewById(Resource.Id.zoomPlus);
            zoomPlus.Click += (s, e) => _zoomLayout.ZoomTo(_zoomLayout.Zoom + 1, true);

            var zoomMinus = view.FindViewById(Resource.Id.zoomMinus);
            zoomMinus.Click += (s, e) => _zoomLayout.ZoomTo(_zoomLayout.Zoom - 1, true);

            var map = view.FindViewById<ConstraintLayout>(Resource.Id.zoomContent);
            view.ViewTreeObserver.GlobalLayout += OnFirstLayout;
            void OnFirstLayout(object sender, System.EventArgs e)
            {
                if (view.Height > 0 && view.Width > 0)
                {
                    view.ViewTreeObserver.GlobalLayout -= OnFirstLayout;
                    map.LayoutParameters.Width = view.Width;
                    map.LayoutParameters.Height = view.Height;
                    map.RequestLayout();
                }
            }

            _hexBackground = view.FindViewById<HexGridView>(Resource.Id.hexGridBackground);
            _hexBackground.CellRadius = 30;

            _hexGrid = view.FindViewById<HexLayout>(Resource.Id.hexGridLayout);
            _hexGrid.CellRadius = 30;
            _hexGrid.Adapter = new HexLayoutAdapter<int>(new int[][]
            {
                    new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },
                new int[] { 0, 0, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 0 },
                    new int[] { 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1 },
                new int[] { 0, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 0 },
                    new int[] { 0, 1, 1, 1, 1, 1, 1, 3, 1, 0, 1, 0 },
                new int[] { 0, 4, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0 },
                    new int[] { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            }, (row, col, element) =>
            {
                if (element == 0)
                    return null;

                if (element >= 2)
                {
                    var v = inflater.Inflate(Resource.Layout.hexagon_notification, _hexGrid, false);
                    v.TranslationZ = 100;
                    v.Click += (s, e) =>
                    {
                        ParentFragmentManager.BeginTransaction()
                            .Add(Id, Java.Lang.Class.FromType(typeof(SectionFragment)), null)
                            .SetCustomAnimations(Resource.Animation.fragment_fade_enter, Resource.Animation.fragment_fade_exit)
                            .AddToBackStack(null)
                            .Commit();
                    };

                    var badge = v.FindViewById<View>(Resource.Id.badge);

                    badge.Visibility = element >= 3 ? ViewStates.Visible : ViewStates.Gone;

                    return v;
                }
                else
                {
                    var v = new ImageView(Context);
                    v.SetBackgroundResource(Resource.Drawable.hexagon);
                    return v;
                }
            });

            return view;
        }
    }
}