﻿using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Views;
using System;

namespace EvrazDemo.Map
{
    [Register("Sample.Map.HexGridView")]
    public class HexGridView : View
    {
        private readonly Paint _wallPaint = new Paint();
        private readonly Paint _fillPaint = new Paint();

        private float _cellWidth;
        private float _cellHeight;
        private float _cellRadius;

        public HexGridView(Context context) : this(context, null)
        {
            Init();
        }

        protected HexGridView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            Init();
        }

        public HexGridView(Context context, Android.Util.IAttributeSet attrs) : base(context, attrs)
        {
            Init();
        }

        public HexGridView(Context context, Android.Util.IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Init();
        }

        public HexGridView(Context context, Android.Util.IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            Init();
        }

        public void Init()
        {
            _wallPaint.Color = Color.Rgb(229, 229, 229);
            _wallPaint.SetStyle(Paint.Style.Stroke);
            _wallPaint.StrokeWidth = 2f;

            _fillPaint.Color = Color.Transparent;
            _fillPaint.SetStyle(Paint.Style.Fill);

            SetCellRadius(50);
        }

        public float CellRadius
        {
            get => _cellRadius;
            set
            {
                SetCellRadius(value);
                Invalidate();
            }
        }

        private void SetCellRadius(float cellRadius)
        {
            _cellRadius = cellRadius;
            _cellWidth = HexMapCalculator.CellWidth((int)cellRadius);
            _cellHeight = HexMapCalculator.CellHeight((int)cellRadius);
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            canvas.DrawColor(Color.Transparent);

            float evenCols, oddCols;

            var fittingCols = (float)Math.Ceiling(canvas.Width / (double)_cellWidth);
            if (fittingCols % 2 == 0)
            {
                evenCols = fittingCols;
                oddCols = fittingCols + 1;
            }
            else
            {
                oddCols = fittingCols;
                evenCols = fittingCols + 1;
            }

            var rows = (int)Math.Ceiling(((canvas.Height / _cellRadius) - 0.5f) / 1.5f);
            if (rows % 2 == 0)
            {
                rows++;
            }

            var hexGridHeight = HexMapCalculator.TotalHeight((int)_cellRadius, rows);

            var xOddOffset = (canvas.Width - _cellWidth * oddCols) / 2;
            var xEvenOffset = (canvas.Width - _cellWidth * evenCols) / 2;
            var yOffset = (canvas.Height - hexGridHeight) / 2;

            var firstIsEven = rows / 2 % 2 == 1;

            var hexPath = BuildHexPath(_cellRadius,
                _cellWidth / 2 + (firstIsEven ? xEvenOffset : xOddOffset),
                _cellHeight / 2 + yOffset);

            for (int i = 0; i < rows; i++)
            {
                var isEvenRow = (i + (firstIsEven ? 0 : 1)) % 2 == 0;
                var cols = isEvenRow ? evenCols : oddCols;

                var rowCellPath = new Path(hexPath);

                for (int j = 0; j < cols; j++)
                {
                    canvas.DrawPath(rowCellPath, _fillPaint);
                    canvas.DrawPath(rowCellPath, _wallPaint);

                    rowCellPath.Offset(_cellWidth, 0);
                }

                hexPath.Offset(isEvenRow ? (xEvenOffset - xOddOffset) : (xOddOffset - xEvenOffset), _cellHeight * 0.75f);
            }
        }

        private Path BuildHexPath(float size, float centerX, float centerY)
        {
            var path = new Path();

            for (var i = 0; i <= 6; i++)
            {
                var angleDeg = (60 * i) - 30;
                var angleRad = Math.PI / 180 * angleDeg;
                var x = (float)(centerX + (size * Math.Cos(angleRad)));
                var y = (float)(centerY + (size * Math.Sin(angleRad)));
                if (i == 0)
                {
                    path.MoveTo(x, y);
                }
                else
                {
                    path.LineTo(x, y);
                }
            }

            return path;
        }
    }
}