﻿using Android.Views;

namespace EvrazDemo.Map
{
    internal interface IHexLayoutAdapter
    {
        View[][] Views { get; }
        int Rows { get; }
        int EvenCols { get; }
        int OddCols { get; }
    }
}