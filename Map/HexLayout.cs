﻿using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;

namespace EvrazDemo.Map
{
    [Register("Sample.Map.HexLayout")]
    internal class HexLayout : FrameLayout
    {
        private int _cellRadius = 50;
        private IHexLayoutAdapter _adapter;
        private float _overlap = 1.5f;

        protected HexLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public HexLayout(Context context) : base(context)
        {
        }

        public HexLayout(Context context, Android.Util.IAttributeSet attrs) : base(context, attrs)
        {
        }

        public HexLayout(Context context, Android.Util.IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public HexLayout(Context context, Android.Util.IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        public IHexLayoutAdapter Adapter
        {
            get => _adapter;
            set
            {
                RemoveAllViews();

                _adapter = value;

                foreach (var viewRow in _adapter.Views)
                {
                    foreach (var view in viewRow)
                    {
                        if (view != null)
                        {
                            AddView(view);
                        }
                    }
                }
            }
        }

        public int CellRadius
        {
            get => _cellRadius;
            set
            {
                _cellRadius = value;
                Invalidate();
            }
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            if (Adapter == null)
                return;

            var cellWidth = (float)(_cellRadius * Math.Sqrt(3));
            var cellHeight = _cellRadius * 2;

            var selfHeight = HexMapCalculator.TotalHeight(_cellRadius, Adapter.Rows);
            var selfWidth = HexMapCalculator.TotalWidth(cellWidth, Adapter.OddCols, Adapter.EvenCols);

            var childWidth = MeasureSpec.MakeMeasureSpec((int)(cellWidth + 2 * _overlap), MeasureSpecMode.Exactly);
            var childHeight = MeasureSpec.MakeMeasureSpec((int)(cellHeight + 2 * _overlap), MeasureSpecMode.Exactly);

            for (int i = 0; i < ChildCount; i++)
            {
                var child = GetChildAt(i);
                child.Measure(childWidth, childHeight);
            }

            SetMeasuredDimension((int)selfWidth, (int)selfHeight);
        }

        protected override void OnLayout(bool changed, int left, int top, int right, int bottom)
        {
            base.OnLayout(changed, left, top, right, bottom);

            if (Adapter == null)
                return;

            var height = bottom - top;
            var width = right - left;

            var cellWidth = HexMapCalculator.CellWidth(_cellRadius);
            var cellHeight = HexMapCalculator.CellHeight(_cellRadius);

            var hexGridHeight = HexMapCalculator.TotalHeight(_cellRadius, Adapter.Rows);
            var xOddOffset = (width - cellWidth * Adapter.OddCols) / 2;
            var xEvenOffset = (width - cellWidth * Adapter.EvenCols) / 2;
            var yOffset = (height - hexGridHeight) / 2;

            for (int i = 0; i < Adapter.Rows; i++)
            {
                var isEvenRow = i % 2 == 0;
                var cols = isEvenRow ? Adapter.EvenCols : Adapter.OddCols;

                for (int j = 0; j < cols; j++)
                {
                    var l = (isEvenRow ? xEvenOffset : xOddOffset) + (j * cellWidth) - _overlap;
                    var r = l + cellWidth + _overlap + 0.5;
                    var t = yOffset + i * cellHeight * 0.75f - _overlap;
                    var b = t + cellHeight + _overlap + 1;

                    var view = Adapter.Views[i][j];
                    view?.Layout((int)l, (int)t, (int)r, (int)b);
                }
            }
        }
    }
}