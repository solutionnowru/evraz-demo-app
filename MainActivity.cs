﻿using Android.App;
using Android.OS;
using AndroidX.AppCompat.App;
using Google.Android.Material.BottomNavigation;
using EvrazDemo.IncidentList;
using EvrazDemo.Map;
using System;

namespace EvrazDemo
{
    [Activity(
        Label = "@string/app_name",
        Theme = "@style/AppTheme",
        MainLauncher = true,
        ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.main_activity);
            var bottomNavigation = FindViewById<BottomNavigationView>(Resource.Id.bottomNavigation);

            bottomNavigation.SelectedItemId = Resource.Id.map;
            bottomNavigation.NavigationItemSelected += (s, e) =>
            {
                Type fragmentType = null;

                switch (e.Item.ItemId)
                {
                    case Resource.Id.map:
                        fragmentType = typeof(MapFragment);
                        break;

                    case Resource.Id.incidents:
                        fragmentType = typeof(AllIncidentsFragment);
                        break;

                    default:
                        fragmentType = typeof(ToBeImplementedFragment);
                        break;
                }

                SupportFragmentManager.BeginTransaction()
                    .Replace(Resource.Id.fragment_container, Java.Lang.Class.FromType(fragmentType), null)
                    .Commit();

                e.Handled = true;
            };
        }
    }
}