﻿using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using AndroidX.RecyclerView.Widget;
using ObservableRecycler;
using EvrazDemo.IncidentDetails;
using System;
using System.Linq;
using System.Reactive.Disposables;

namespace EvrazDemo.IncidentList
{
    [Register("Sample.IncidentList.IncidentListFragment")]
    public class IncidentListFragment : Fragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var incidentList = inflater.Inflate(Resource.Layout.incident_list, container, false) as RecyclerView;
            incidentList.AddItemDecoration(new DividerItemDecoration(Context, LinearLayoutManager.Vertical)
            {
                Drawable = Context.GetDrawable(Resource.Drawable.divider)
            });

            var incidentListModel = Enumerable.Range(0, 10)
                .Select(i => new Incident(
                    DateTime.Now.AddDays(-1).AddMinutes((-i * 90) - i).AddSeconds(-i * 4),
                    "Название дивизиона и сегмента происшествия",
                    "Во время выполнения работ на конвертере № 3 произошел хлопок с последующим выбросом пара и горячего воздуха.")
                {
                    WasRead = i > 2
                })
                .ToList();

            incidentList.SetAdapter(new ObservableRecyclerAdapter<Incident, CachingViewHolder>
            {
                DataSource = incidentListModel,
                CellLayoutId = Resource.Layout.incident_list_item,
                BindViewHolderDelegate = OnBindIncidentItem,
                ClickCallback = OnIncidentItemClick
            });

            return incidentList;
        }

        private void OnIncidentItemClick(CachingViewHolder holder, Incident model, int position)
        {
            Activity.SupportFragmentManager.BeginTransaction()
                .Add(Resource.Id.fragment_container, Java.Lang.Class.FromType(typeof(IncidentDetailsFragment)), null)
                .AddToBackStack(null)
                .Commit();
        }

        private void OnBindIncidentItem(CachingViewHolder holder, Incident model, int position, CompositeDisposable disposable)
        {
            var date = holder.FindCachedView<TextView>(Resource.Id.dateTime);
            var place = holder.FindCachedView<TextView>(Resource.Id.place);
            var description = holder.FindCachedView<TextView>(Resource.Id.description);
            var indicator = holder.FindCachedView<ImageView>(Resource.Id.indicator);

            date.Text = model.Date.ToString("dd.MM.yyyy hh:mm");
            place.Text = model.Place;
            description.Text = model.Description;
            indicator.SetImageResource(model.WasRead ? Resource.Drawable.ic_indicator_clear : Resource.Drawable.ic_indicator_active);
        }
    }
}