﻿using System;

namespace EvrazDemo.IncidentList
{
    internal class Incident
    {
        public Incident(DateTime date, string place, string description)
        {
            Date = date;
            Place = place;
            Description = description;
        }

        public DateTime Date { get; }
        public string Place { get; }
        public string Description { get; }
        public bool WasRead { get; set; }
    }
}