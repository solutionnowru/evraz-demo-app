﻿using Android.OS;
using Android.Views;
using AndroidX.Fragment.App;

namespace EvrazDemo.IncidentList
{
    public class AllIncidentsFragment : Fragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.all_incidents_fragment, container, false);

            return view;
        }
    }
}